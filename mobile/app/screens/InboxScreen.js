import React, {Component} from 'react';
import {View, Text} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

import {Container} from '../components/Container';

class InboxScreen extends Component {
  render() {
    return (
      <Container>
        <Text>InboxScreen</Text>
      </Container>
    );
  }
}

InboxScreen.navigationOptions = () => ({
  title: 'Inbox',
});

const styles = EStyleSheet.create({});

export default InboxScreen;
