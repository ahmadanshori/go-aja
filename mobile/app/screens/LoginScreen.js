import React, {useState} from 'react';
import {View, Text, ImageBackground, Dimensions} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Icon from 'react-native-vector-icons/Ionicons';
import * as Animatable from 'react-native-animatable';

import {
  useNavigation,
  useNavigationParam,
  useNavigationState,
  useNavigationKey,
  useNavigationEvents,
  useFocusState,
} from '../components/hooks/Hooks';
import {Login} from '../components/Login';
import {Button} from '../components/Button';
import {Container} from '../components/Container';

const LoginScreen = () => {
  const {navigate} = useNavigation();
  const [show, setShow] = useState(false);
  const [button, setButton] = useState(true);

  handleClose = () => {
    setButton(true);
    setShow(false);
  };
  handleButton = () => {
    setButton(false);
    setShow(true);
  };
  console.log('show', show);
  console.log('button', button);
  return (
    <Container style={styles.container}>
      <ImageBackground
        source={require('../assets/img/login.jpg')}
        style={styles.img}>
        {show && button == false ? (
          <Animatable.View
            style={{position: 'absolute', bottom: 0}}
            animation="fadeInUpBig"
            easing="ease">
            <Login onPressClose={() => handleClose()} />
          </Animatable.View>
        ) : (
          <Animatable.View
            style={{position: 'absolute', bottom: 0}}
            animation="fadeOutDownBig"
            easing="ease">
            <Login />
          </Animatable.View>
        )}
        {button && show == false ? (
          <Animatable.View
            style={styles.containerButton}
            animation="fadeInUpBig"
            easing="ease-in">
            <Button text="SIGN IN" onPress={() => handleButton()} />
            <Button text="SIGN WITH GOOGLE" onPress={() => navigate('App')} />
          </Animatable.View>
        ) : (
          <Animatable.View
            style={styles.containerButton}
            animation="fadeOutDownBig"
            easing="ease-out">
            <Button text="SIGN IN" />
            <Button text="SIGN WITH GOOGLE" />
          </Animatable.View>
        )}
      </ImageBackground>

      {/* <Text>LoginScreen</Text>
      <Button title="button" onPress={() => navigate('App')} /> */}
    </Container>
  );
};

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerButton: {
    // paddingVertical: 5,
  },
  img: {
    width: '$screenWidth',
    height: '$screenHeight',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default LoginScreen;
