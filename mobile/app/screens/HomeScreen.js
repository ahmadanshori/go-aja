import React, {Component} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Icon from 'react-native-vector-icons/Ionicons';

import {Category} from '../components/Category';

import {Container} from '../components/Container';

const HomeScreen = () => {
  return (
    <Container style={styles.container}>
      <Category />
    </Container>
  );
};

HomeScreen.navigationOptions = () => ({
  title: 'Home',
});
const styles = EStyleSheet.create({
  container: {
    marginTop: 30,
  },
});

export default HomeScreen;
