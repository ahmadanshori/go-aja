import React, {Component} from 'react';
import {View, Text} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

import {Container} from '../components/Container';

class ChatScreen extends Component {
  render() {
    return (
      <Container>
        <Text>ChatScreen</Text>
      </Container>
    );
  }
}

ChatScreen.navigationOptions = () => ({
  title: 'Chat',
});

const styles = EStyleSheet.create({});

export default ChatScreen;
