import React, {Component} from 'react';
import {View, Text} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

import {Container} from '../components/Container';

class AccountScreen extends Component {
  render() {
    return (
      <Container>
        <Text>AccountScreen</Text>
      </Container>
    );
  }
}
AccountScreen.navigationOptions = () => ({
  title: 'Account',
});

const styles = EStyleSheet.create({});

export default AccountScreen;
