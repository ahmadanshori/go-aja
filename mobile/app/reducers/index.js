import {combineReducers} from 'redux';
import order from './OrderReducer';

const rootReducer = combineReducers({
  order,
});

// export default (state, action) => {
//   if (action.type == "CLEAR_DATA") {
//     console.log("testt");
//     // state = {}
//   }
//   return rootReducer(state, action);
// };

export default rootReducer;
