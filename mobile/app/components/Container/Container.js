import React from 'react';
import PropTypes from 'prop-types';
import {View, StatusBar} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

const Container = ({children, backgroundColor, style = {}}) => {
  const containerStyles = [[styles.container, style]];
  if (backgroundColor) {
    containerStyles.push({backgroundColor});
  }
  return (
    <View style={containerStyles}>
      <StatusBar
        backgroundColor={EStyleSheet.value('$black')}
        barStyle="light-content"
      />
      {children}
    </View>
  );
};

const styles = EStyleSheet.create({
  container: {
    backgroundColor: '$white',
    width: '$screenWidth',
    height: '100%',
  },
});
Container.propTypes = {
  children: PropTypes.any,
  backgroundColor: PropTypes.string,
};

export default Container;
