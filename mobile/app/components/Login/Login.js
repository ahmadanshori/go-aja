import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  Text,
  Dimensions,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Icon from 'react-native-vector-icons/Ionicons';
import * as Animatable from 'react-native-animatable';

const Height = Dimensions.get('window').height;
const Width = Dimensions.get('window').width;

const Login = ({onPressClose, position}) => {
  return (
    <View style={styles.login}>
      <TouchableOpacity
        style={styles.containerIcon}
        activeOpacity={1}
        onPress={onPressClose}>
        <Icon name="md-close" size={30} />
      </TouchableOpacity>
      <View style={styles.containerTextInput}>
        <TextInput style={styles.textInput} placeholder="EMAIL" />
        <TextInput style={styles.textInput} placeholder="PASSWORD" />
      </View>
      <TouchableOpacity style={styles.button}>
        <Text style={styles.text}>SIGN IN</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = EStyleSheet.create({
  login: {
    height: Height / 2.5,
    width: Width,
    backgroundColor: 'white',
    // justifyContent: 'center',
    alignItems: 'center',
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
  },
  containerIcon: {
    height: 40,
    width: 40,
    backgroundColor: '$white',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    position: 'absolute',
    top: -20,
    left: Width / 2 - 20,
    shadowColor: 'rgba(0,0,0, 0.4)',
    shadowOffset: {height: 0, width: 4},
    shadowOpacity: 0.6,
    shadowRadius: 4,
    elevation: 5,
  },
  containerTextInput: {
    marginTop: 50,
  },
  textInput: {
    // backgroundColor: 'rgba(59,137,157, 0.2)',
    borderBottomWidth: 0.3,
    borderColor: 'rgb(59,137,157)',
    height: 50,
    width: Width - 20,
    borderRadius: 25,
    paddingLeft: 30,

    // color: '$white',
    // backgroundColor: '$white',
  },
  text: {
    fontWeight: 'bold',
  },
  button: {
    borderWidth: 1,
    height: 60,
    width: Width - 20,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
    backgroundColor: '$white',
    borderColor: 'transparent',
    shadowColor: 'rgba(0,0,0, 0.4)',
    shadowOffset: {height: 0, width: 4},
    shadowOpacity: 0.6,
    shadowRadius: 4,
    elevation: 5,
  },
});

export default Login;
