import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
import AntDesign from 'react-native-vector-icons/AntDesign';

const Category = () => {
  return (
    <View style={styles.container}>
      <View style={styles.top}>
        <View style={styles.containerIcon}>
          <Icon name="wallet" size={15} color="white" />
          <Text style={styles.text}>gopay</Text>
        </View>
        <View>
          <Text style={styles.text}>Rp 10.000</Text>
        </View>
      </View>
      <View style={styles.bottom}>
        <TouchableOpacity style={styles.option}>
          <AntDesign name="up-square-o" color="white" size={30} />
          <Text style={styles.text}>Pay</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.option}>
          <AntDesign name="closesquareo" color="white" size={30} />
          <Text style={styles.text}>Promo</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.option}>
          <AntDesign name="plussquareo" color="white" size={30} />
          <Text style={styles.text}>Top Up</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.option}>
          <AntDesign name="minussquareo" color="white" size={30} />
          <Text style={styles.text}>More</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '20%',
    marginHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },
  top: {
    height: '30%',
    width: '100%',
    flexDirection: 'row',
    backgroundColor: 'rgb(54,96,178)',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
  },
  containerIcon: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: 'white',
    fontSize: 13,
  },
  bottom: {
    height: '70%',
    width: '100%',
    backgroundColor: 'rgb(37,99,181)',
    flexDirection: 'row',
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
  },
  option: {
    width: '25%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Category;
