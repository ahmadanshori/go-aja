import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';

import {
  createAppContainer,
  createStackNavigator,
  createSwitchNavigator,
  createBottomTabNavigator,
} from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import HomeScreen from '../screens/HomeScreen';
import OrderScreen from '../screens/OrderScreen';
import ChatScreen from '../screens/ChatScreen';
import InboxScreen from '../screens/InboxScreen';
import AccountScreen from '../screens/AccountScreen';

import LoginScreen from '../screens/LoginScreen';

const HomeStack = createStackNavigator({HomeScreen});
const OrderStack = createStackNavigator({OrderScreen});
const ChatStack = createStackNavigator({ChatScreen});
const InboxStack = createStackNavigator({InboxScreen});
const AccountStack = createStackNavigator({AccountScreen});

const TabNav = createBottomTabNavigator(
  {
    Home: {
      screen: HomeStack,
      navigationOptions: ({navigation}) => ({
        tabBarLabel: 'Home',
        tabBarIcon: ({tintColor}) => (
          <Icon name="md-home" size={25} color={tintColor} />
        ),
      }),
    },
    ListOrder: {
      screen: OrderStack,
      navigationOptions: {
        tabBarLabel: 'Order',
        tabBarIcon: ({tintColor}) => (
          <Icon name="ios-list-box" size={25} color={tintColor} />
        ),
      },
    },
    Chat: {
      screen: ChatStack,
      navigationOptions: {
        tabBarLabel: 'Chat',
        tabBarIcon: ({tintColor}) => (
          <MaterialCommunityIcons
            name="chat-processing"
            size={25}
            color={tintColor}
          />
        ),
      },
    },
    Inbox: {
      screen: InboxStack,
      navigationOptions: {
        tabBarLabel: 'Inbox',
        tabBarIcon: ({tintColor}) => (
          <MaterialIcons name="inbox" size={25} color={tintColor} />
        ),
      },
    },
    Account: {
      screen: AccountStack,
      navigationOptions: {
        tabBarLabel: 'Account',
        tabBarIcon: ({tintColor}) => (
          <MaterialIcons name="account-box" size={25} color={tintColor} />
        ),
      },
    },
  },
  {
    tabBarOptions: {
      headerMode: 'none',
      activeTintColor: 'rgb(76,167,53)',
      inactiveTintColor: 'gray',
      style: {
        borderColor: 'transparent',
        shadowColor: '$blackShadow',
        shadowOffset: {height: 0, width: 2},
        shadowOpacity: 0.5,
        shadowRadius: 3,
        elevation: 20,
      },
    },
  },
);

const RootNavigations = createSwitchNavigator({
  Login: {
    screen: LoginScreen,
    navigationOptions: {
      header: null,
    },
  },
  App: TabNav,
});

export default createAppContainer(RootNavigations);
