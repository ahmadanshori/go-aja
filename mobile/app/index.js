import React, {Component} from 'react';
import {Dimensions, BackHandler} from 'react-native';
// import {Provider, connect} from 'react-redux';
import EStyleSheet from 'react-native-extended-stylesheet';
// import store from './config/store';
import Navigator from './config/routes';
import Home from './screens/HomeScreen';

EStyleSheet.build({
  $darkRed: '#990500',
  $white: '#FFFFFF',
  $green: 'rgb(76,167,53)',
  $darkGreen: '#098716',
  $red: 'rgb(212,55,65)',
  $yellow: 'rgb(210,163,43)',
  $gray: 'rgb(194,194,194)',
  $black: '#000000',
  $screenWidth: Dimensions.get('window').width,
  $screenHeight: Dimensions.get('window').height,

  //   $outline: 1,
});

export default () => <Navigator />;
